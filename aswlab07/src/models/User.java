package models;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import util.MD5Encoder;

public class User {

	private Integer user_id = -1;
	private String user_name;
	private String password;
	public Integer getUser_id() {
		return user_id;
	}
	private void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}
	public String getUser_name() {
		return user_name;
	}
	private void setUser_name(String user_name) {
		this.user_name = user_name;
	}
	private String getPassword() {
		return password;
	}
	private void setPassword(String password) {
		this.password = password;
	}
	
	public Boolean passwordMatch(String pass){
		return this.getPassword().equals(MD5Encoder.encode(pass));
	}

	public static User findByName(Connection dbConnection, String name) throws ModelException {
		User user = null;
		String  queryString = "select * from users where usNAME = ?";
		try {
			PreparedStatement stmt = dbConnection.prepareStatement(queryString);
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			if (rs.first()) {
				user = new User();
				user.setUser_id(rs.getInt(1));
				user.setUser_name(rs.getString(2));
				user.setPassword(rs.getString(3));
			}
		}
		catch (SQLException ex) { throw new ModelException(ex); }
		return user;
	}

	
	public static User findById(Connection dbConnection, Integer id) throws ModelException {
		User user = null;
		String  queryString = "select * from users where usID = ?";
		try {
			PreparedStatement stmt = dbConnection.prepareStatement(queryString);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			if (rs.first()) {
				user = new User();
				user.setUser_id(rs.getInt(1));
				user.setUser_name(rs.getString(2));
				user.setPassword(rs.getString(3));
			}
		}
		catch (SQLException ex) { throw new ModelException(ex); }
		return user;
	}
	
	public static User create(Connection dbConnection, String user_name, String password) throws ModelException {
		User user = null;
		try {
			String insert = "insert into users(usNAME, usPASSW) values (?, ?)";
			PreparedStatement stmt = dbConnection.prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, user_name);
			stmt.setString(2, MD5Encoder.encode(password));
			stmt.executeUpdate();
			ResultSet keys = stmt.getGeneratedKeys();
			keys.first();
			user = User.findById(dbConnection, keys.getInt(1));
			keys.close();
			stmt.close();
		}
		catch (SQLException ex ) {
			if (ex.getErrorCode() == 23505) {  // Username ja existeix a la BD
				throw new ModelException("Username '"+user_name+"' is already taken by another user");
			}
			else throw new ModelException(ex);
		}

		return user;
	}
	
}
