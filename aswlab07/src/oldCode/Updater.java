
package oldCode;

import java.io.IOException;
import java.sql.Connection;
import java.util.Vector;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.ModelException;
import models.Tweet;
import models.User;
import net.tanesha.recaptcha.ReCaptcha;
import net.tanesha.recaptcha.ReCaptchaFactory;
import net.tanesha.recaptcha.ReCaptchaResponse;
import wallDB.Parameters;

public class Updater extends HttpServlet {

	/**
	 *
	 */
	private static final long serialVersionUID = -5825217906570443055L;

	/* (non-Javadoc)
	 * @see javax.servlet.GenericServlet#init()
	 */
	@Override

	public void service (HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {

		boolean isOK = false;
		Vector<String> errorList = new Vector<String>();
		try {
			String action = req.getParameter("action");
			if (action != null)
				if (action.equals("login"))
					isOK = login(req, errorList);
				else if (action.equals("delete"))
					isOK = delete (req, errorList);
				else if (action.equals("Tweet!"))
					isOK = insert(req, errorList);
				else if (action.equals("Sign Up")) 
					isOK = register(req, errorList);
		}
		catch (ModelException ex) {
			errorList = ex.getMessageList();
		}
		if (isOK)
			res.sendRedirect("wall");
		else {

			req.setAttribute("theList", errorList);
			RequestDispatcher dispatch = req.getRequestDispatcher("error.sp");
			dispatch.forward(req, res);
		}

	}

	public boolean delete(HttpServletRequest req, Vector<String>  out)
			throws ModelException {
		Integer userID = (Integer) req.getSession().getAttribute("loggedUserID");
		String password = req.getParameter("userpasswd");
		String[] tweetIDs = req.getParameterValues("tweetids");
		boolean deleteOK = false;

		if (tweetIDs != null) {
			Connection dbConnection = (Connection) this.getServletContext().getAttribute("dbconnection");
			User user = User.findById(dbConnection, userID);
			if (user.passwordMatch(password)){
				for (String tid : tweetIDs) {
					Tweet tweet = Tweet.findById(dbConnection, Integer.valueOf(tid));
					tweet.delete(dbConnection);				
				}
				deleteOK = true;
			}
			else 
				out.addElement("Incorrect password");
		}
		else
			out.addElement("No items selected to delete");
		return deleteOK;
	}


	public boolean login(HttpServletRequest req, Vector<String>  out) throws ModelException {

		String userName = req.getParameter("login_username");
		String password = req.getParameter("login_password");
		boolean logged = false;

		Connection dbConnection = (Connection) this.getServletContext().getAttribute("dbconnection");
		User user = User.findByName(dbConnection, userName);

		if (user == null) 
			out.addElement("No user with this username");
		else {
			if (! user.passwordMatch(password)) 
				out.addElement("Wrong password: " + password);
			else {
				req.getSession().setAttribute("loggedUserNAME",userName);
				req.getSession().setAttribute("loggedUserID", user.getUser_id());
				logged = true;
			}
		}
		return logged;
	}

	private boolean insert(HttpServletRequest req, Vector<String>  out)
			throws ModelException
			{

		String content = req.getParameter("tweet_content");
		Integer userID = (Integer) req.getSession().getAttribute("loggedUserID");
		boolean insertionOK = false;

		Boolean paramsOK = true;

		if (userID == null) { out.addElement("You must login first"); paramsOK = false; }
		if (content == null || content.equals(""))  { out.addElement("Content cannot be empty"); paramsOK = false;}

		if (paramsOK)
		{ 
			Connection dbConnection = (Connection) this.getServletContext().getAttribute("dbconnection");
			Tweet.create(dbConnection, userID, content);
			insertionOK = true;
		}
		return insertionOK;
			}

	private boolean register(HttpServletRequest req, Vector<String>  out)
			throws ModelException {

		String user_name = req.getParameter("reg_username");
		String pwd = req.getParameter("reg_password");
		boolean registered = false;

		Boolean paramsOK = true;

		if (user_name.equals("")) { out.addElement("Username cannot be empty"); paramsOK = false;}
		if (pwd.equals(""))  { out.addElement("Password cannot be empty"); paramsOK = false;}

		ReCaptcha captcha = ReCaptchaFactory.newReCaptcha(Parameters.getString("recaptcha.publicKey"), Parameters.getString("recaptcha.privateKey"), false);
		ReCaptchaResponse response = captcha.checkAnswer(req.getRemoteAddr(), req.getParameter("recaptcha_challenge_field"), req.getParameter("recaptcha_response_field"));

		if (!response.isValid())  { out.addElement("Incorrect captcha values"); paramsOK = false;}

		if (paramsOK) {
			Connection dbConnection = (Connection) this.getServletContext().getAttribute("dbconnection");
			User new_user = User.create(dbConnection, user_name, pwd);
			req.getSession().setAttribute("loggedUserNAME",user_name);
			req.getSession().setAttribute("loggedUserID",new_user.getUser_id());
			registered = true;

		}

		return registered;
	}
}
