package springFront;

import java.sql.Connection;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import models.ModelException;
import models.User;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
public class LogInOutController {

	private static final Logger logger = LoggerFactory.getLogger(LogInOutController.class);

	@RequestMapping(value = "/logout", method = RequestMethod.POST)
	public  String logout(HttpSession session) {

		logger.info("Logging out user with username = "+ session.getAttribute("loggedUserNAME")) ;

		session.setAttribute("loggedUserNAME",null);
		session.setAttribute("loggedUserID",null);
		return ("redirect:/");
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public  String login(@Valid @ModelAttribute("loginForm") LoginForm loginForm, BindingResult binding, 
			HttpSession session, Model model) {

		logger.info("Logging ...");

		if (binding.hasErrors()) {
			logger.info("Logging fails: form validation errors");
			return "browser";
		}

		String username = loginForm.getLogin_username();
		String password = loginForm.getLogin_password();
		Connection dbConnection = (Connection) session.getServletContext().getAttribute("dbconnection");

		String toreturn = "browser";

		try {

			User user = User.findByName(dbConnection, username);

			if (user == null) {
				binding.rejectValue("login_username", "wrongValue", "There is no such a user");
				logger.info("Login fails: there is no such a user");
			}
			else {
				if (! user.passwordMatch(password)) {
					binding.rejectValue("login_password", "wrongValue", "Wrong password!");
					logger.info("Login fails: wrong password");
				}
				else {
					session.setAttribute("loggedUserNAME",username);
					session.setAttribute("loggedUserID", user.getUser_id());
					toreturn = "redirect:/";
					logger.info("User successfully logged");
				}
			}	

			return toreturn;
		}
		catch (ModelException ex) {

			model.addAttribute("theList",ex.getMessageList());
			return ("error");
		}
	}
}
