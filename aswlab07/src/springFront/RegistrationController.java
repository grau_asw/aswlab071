package springFront;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


@Controller
@RequestMapping("/registration")
public class RegistrationController {

	private static final Logger logger = LoggerFactory.getLogger(RegistrationController.class);

	@InitBinder
    protected void initBinder(HttpServletRequest request, WebDataBinder binder) {
        binder.setValidator(new RegFormValidator(request));
    }
	
	@RequestMapping(method = RequestMethod.GET)
	public String setupForm(Model model) {
		return "user_registration";
	}
	
	@ModelAttribute("registrationForm")
	public RegistrationForm newRegistrationForm() {
		RegistrationForm regForm = new RegistrationForm();
		return regForm;
	}

	@RequestMapping(method = RequestMethod.POST)
	public  String register(/* Poseu els parÓmetres vosaltres mateixos*/) 
	{
		logger.info("Registring new  user");
		
		return "Alguna cosa";

	}



}
